```Improvement ideas:```

# Sidemenu
Sectiune de profile (account) => schimbare parola, date (update)

Nume utilizator

Submeniuri (Settings?) - Application settings, About

Dark mode!!!!

Badge

History per user

Report

Scor de incredere

# Map
buton pentru centrare

# Shared Links
## Week 2
- Visual Studio Code: https://code.visualstudio.com/
- Prettier - https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode
- History API - https://developer.mozilla.org/en-US/docs/Web/API/History
- React Router - https://reacttraining.com/react-router/web/api/NavLink
- React Styledcomponents - https://styled-components.com/
- CSS Variables - https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties 
- SASS Variables - https://sass-lang.com/documentation/variables
- NPM React Geolocated - https://www.npmjs.com/package/react-geolocated
- Promise API - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise
- Javascript Const Immutability - https://dev.to/valentinogagliardi/once-and-for-all-const-in-javascript-is-not-immutable-2gop
- Javascript Const Immutability 2 - https://alligator.io/js/const-vs-obj-freeze/

## Week 3
#### JS
- Axios - https://github.com/axios/axios
- CanIUse - Feature comparison in different browser https://caniuse.com/ 
- https://capacitor.ionicframework.com/docs/apis/geolocation/#method-getCurrentPosition-0
- https://uber.github.io/react-map-gl/examples/clusters Clusters on the Map
- Clusters on the map examples: https://github.com/uber/react-map-gl/tree/5.2-release/examples/clusters

#### CSS
- http://getbem.com/naming/ - BEM
- https://clubmate.fi/oocss-acss-bem-smacss-what-are-they-what-should-i-use/ - ALL the CSS style notations
- http://smacss.com/ - Another CSS Notation system
- CSS Attribute Selector - https://www.w3schools.com/css/css_attribute_selectors.asp
- Reconciliation - https://reactjs.org/docs/reconciliation.html
- Collapsable Margins in CSS - https://css-tricks.com/what-you-should-know-about-collapsing-margins/
- Collapsable Margins in CSS V2 - https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Box_Model/Mastering_margin_collapsing



#### Fun CSS Games
- https://flukeout.github.io/ - Fun times with everyone
- https://flexboxfroggy.com/ - Flexbox like a boss

## Week 4
- https://about.gitlab.com/blog/2016/12/01/how-to-keep-your-fork-up-to-date-with-its-origin/ Fork Sync with Master 
- https://reactjs.org/docs/context.html#consuming-multiple-contexts React Context API and How to consume multiple cons
- https://nikgraf.github.io/react-hooks/ Different React Hooks
- MomentJS: https://momentjs.com/ (preferinta lui Robert)
- Date FNS v2 - https://github.com/date-fns/date-fns/releases/tag/v2.0.0 (preferinta lui Dinu)

## Week 5
 - https://fontawesome.com/ - SVG Fonts
 - https://gulpjs.com/ - Gulp JS - Task Runner
 - https://webpack.js.org/ - Webpack Build Tool
 - https://javascript.christmas/2019/10 - Yarn vs NPM vs NPX
 - https://capacitor.ionicframework.com/docs/apis/geolocation/ - Capacitor Geolocation
 - https://beautifier.io/ 
 - https://ionicframework.com/docs/developing/android 
 - http://www.automationtestinghub.com/setup-android-environment-variables/ - setup windows environment variables
 - https://www.filehorse.com/download-java-development-kit-64/49906/download/ - JDK
 - https://www.freecodecamp.org/news/npm-vs-npx-whats-the-difference/ - NPX vs NPM
 - https://ionicframework.com/docs/react/your-first-app/6-deploying-mobile - deployment mobile
 - 