import React, { useState, useContext, useEffect } from 'react';
import { IonIcon, IonSpinner } from '@ionic/react';
import { person, add, remove } from 'ionicons/icons';
import moment from 'moment';
import api from '../../api';
import { AuthContext } from '../auth';

import './Event.scss';

const Event = ({ allowParticipate, event, onEventChange }) => {
    const { currentUser } = useContext(AuthContext);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        setLoading(false);
    }, [event]);

    const joinEvent = (event, user) => {
        setLoading(true);
        api.joinEvent(event, user).then(() => {
            onEventChange();
        });
    };

    const leaveEvent = (event, user) => {
        setLoading(true);
        api.leaveEvent(event, user).then(() => {
            onEventChange();
        });
    };

    const renderButton = id => {
        if (userInParticipants(id) && allowParticipate) {
            return (
                <div
                    className={`event__participate event__participate--leave ${loading &&
                        'event__participate--loading'}`}
                >
                    {!loading && (
                        <IonIcon
                            icon={remove}
                            onClick={() => leaveEvent(event.id, id)}
                        />
                    )}
                    {loading && <IonSpinner name="dots" />}
                </div>
            );
        }

        if (allowParticipate) {
            return (
                <div
                    className={`event__participate event__participate--join ${loading &&
                        'event__participate--loading'}`}
                >
                    {!loading && (
                        <IonIcon
                            icon={add}
                            onClick={() => joinEvent(event.id, id)}
                        />
                    )}
                    {loading && <IonSpinner name="dots" />}
                </div>
            );
        }
    };

    const userInParticipants = id => {
        return (
            event.participants.filter(participant => participant.user === id)
                .length > 0
        );
    };

    return (
        <div className="event__container">
            <div className="event__date">
                <span>{moment(event.date).format('DD')}</span>
                <span>{moment(event.date).format('MMMM')}</span>
            </div>
            <div className="event__details">
                <span className="event__name">{event.name}</span>
                <span className="event__participants">
                    <IonIcon icon={person} />
                    <span className="event__participants-counter">
                        {event.participants.length}/{event.max_participants}
                    </span>
                </span>
            </div>
            {renderButton(currentUser.uid)}
        </div>
    );
};

export default Event;
