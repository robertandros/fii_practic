import React from 'react';
import { IonApp } from '@ionic/react';

import AuthProvider from '../../components/auth';
import Router from './router';

const App = () => (
    <IonApp>
        <AuthProvider>
            <Router />
        </AuthProvider>
    </IonApp>
);

export default App;
