import React, { Component } from 'react';
import {
    getPlatforms,
    IonButtons,
    IonHeader,
    IonMenuButton,
    IonPage,
    IonToolbar
} from '@ionic/react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { ROUTE_ACCOUNT, ROUTE_MAP } from '../../../utils/routes';
import Account from '../account';
import Map from '../map';
import Sidemenu from '../../../components/sidemenu';

import './Home.scss';

class Home extends Component {
    render() {
        return (
            <>
                <Sidemenu id="main-content" />
                <IonHeader className="home-header">
                    <IonToolbar>
                        <IonButtons slot="start">
                            <IonMenuButton
                                color="dark"
                                fill="clear"
                                slot="icon-only"
                            />
                        </IonButtons>
                    </IonToolbar>
                </IonHeader>
                <IonPage id="main-content" className="main-content" data-platforms={getPlatforms()}>
                    <Switch>
                        <Route path={ROUTE_ACCOUNT} exact component={Account} />
                        <Route path={ROUTE_MAP} exact component={Map} />
                        <Route
                            path="/home"
                            exact
                            render={() => <Redirect to="/home/map" />}
                        />
                    </Switch>
                </IonPage>
            </>
        );
    }
}

export default Home;
